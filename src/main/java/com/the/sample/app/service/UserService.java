package com.the.sample.app.service;

import com.the.sample.app.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> findAll(int page,int pageSize);
    Optional<User> findById(Long id);
    Optional<User> findByEmail(String email);
    void save(User user);
    void deleteById(Long id);
}
