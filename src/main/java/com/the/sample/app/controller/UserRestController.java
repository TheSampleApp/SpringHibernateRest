package com.the.sample.app.controller;

import com.the.sample.app.model.User;
import com.the.sample.app.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserRestController {
    private final UserService userService;

    @GetMapping("/{id}")
    public Mono<User> getUserById(@PathVariable("id") Long id) {
        return Mono.justOrEmpty(userService.findById(id));
    }

    @GetMapping("/{page}/{pageSize}")
    public Flux<User> getAllUsers(@PathVariable("page") Integer page,
                                  @PathVariable("pageSize") Integer pageSize) {
        return Flux.fromIterable(userService.findAll(page,pageSize));
    }

    @PostMapping("/")
    public Mono<User> saverUser(@RequestBody User user) {
        userService.save(user);
        return Mono.just(user);
    }

    @PutMapping("/{id}")
    public Mono<User> updateUser(@PathVariable("id") Long id,@RequestBody User user) {
        user.setId(id);
        userService.save(user);
        return Mono.just(user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        userService.deleteById(id);
    }
}
